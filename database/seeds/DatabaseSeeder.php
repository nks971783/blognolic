<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('roles')->insert([
            'name' => 'Admin'
        ]);
		DB::table('roles')->insert([
            'name' => 'Author'
        ]);
        DB::table('roles')->insert([
            'name' => 'User'
        ]);
		DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@blognolic.com',
			'role_id' => '1',
            'password' => bcrypt('secret123'),
        ]);
    }
}
