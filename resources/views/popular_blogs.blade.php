@extends('layouts.app')

@section('title', 'Blognolic :: Popular Blogs')

@section('meta_title', 'Home Page Meta Title')
@section('meta_keywords', 'Meta Keyword')
@section('meta_description', 'Meta Description')

@section('content')
        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner">
                                <!--post header-->
                                <div class="post-head">
                                    <h2 class="title"><strong>Most Popular</strong> articles</h2>
                                </div>
                                <!-- post body -->
                                <div class="post-body">
                                    <div class="news-list-item articles-list">
                                        <div class="img-wrapper">
                                            <a href="#" class="thumb"><img src="assets/images/218x150-1.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="post-info-2">
                                            <h4><a href="#" class="title">There are many variations of passages of Lorem Ipsum available, but the majority have</a></h4>
                                            <ul class="authar-info">
                                                <li><i class="ti-timer"></i> May 15, 2016</li>
                                                <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                            </ul>
                                            <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                                        </div>
                                    </div>
                                    <div class="news-list-item articles-list">
                                        <div class="img-wrapper">
                                            <a href="#" class="thumb"><img src="assets/images/218x150-2.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="post-info-2">
                                            <h4><a href="#" class="title">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical</a></h4>
                                            <ul class="authar-info">
                                                <li><i class="ti-timer"></i> May 15, 2016</li>
                                                <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                            </ul>
                                            <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                                        </div>
                                    </div>
                                    <div class="news-list-item articles-list">
                                        <div class="img-wrapper">
                                            <a href="#" class="thumb"><img src="assets/images/218x150-3.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="post-info-2">
                                            <h4><a href="#" class="title">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</a></h4>
                                            <ul class="authar-info">
                                                <li><i class="ti-timer"></i> May 15, 2016</li>
                                                <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                            </ul>
                                            <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                                        </div>
                                    </div>
                                    <div class="news-list-item articles-list">
                                        <div class="img-wrapper">
                                            <a href="#" class="thumb"><img src="assets/images/218x150-4.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="post-info-2">
                                            <h4><a href="#" class="title">It is a long established fact that a reader will be distracted by the readable </a></h4>
                                            <ul class="authar-info">
                                                <li><i class="ti-timer"></i> May 15, 2016</li>
                                                <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                            </ul>
                                            <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                                        </div>
                                    </div>
                                    <div class="news-list-item articles-list">
                                        <div class="img-wrapper">
                                            <a href="#" class="thumb"><img src="assets/images/340x215-1.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="post-info-2">
                                            <h4><a href="#" class="title">Replication For Dummies 4 Easy Steps To Professional DVD</a></h4>
                                            <ul class="authar-info">
                                                <li><i class="ti-timer"></i> May 15, 2016</li>
                                                <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                            </ul>
                                            <p class="hidden-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley...</p>
                                        </div>
                                    </div>
                                </div> <!-- /. post body -->
                                <!-- Post footer -->
                                <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            <!-- pagination -->
                                            <ul class="pagination">
                                                <li class="disabled"><span class="ti-angle-left"></span></li>
                                                <li class="active"><span>1</span></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li class="disabled"><span class="extend">...</span></li><li>
                                                <li><a href="#">12</a></li>
                                                <li><a href="#"><i class="ti-angle-right"></i></a></li>
                                            </ul> <!-- /.pagination -->
                                        </div>
                                    </div>
                                </div> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <div class="col-sm-4 col-p rightSidebar">
                        <div class="theiaStickySidebar">
                            <!-- START SOCIAL ICON -->
                            <div class="social-media-inner">
                                <ul class="social-media clearfix">
                                    <li>
                                        <a href="#" class="rss">
                                            <i class="fa fa-rss"></i>
                                            <div>2,035</div>
                                            <p>Subscribers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="fb">
                                            <i class="fa fa-facebook"></i>
                                            <div>3,794</div>
                                            <p>Fans</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="g_plus">
                                            <i class="fa fa-google-plus"></i>
                                            <div>941</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="you_tube">
                                            <i class="fa fa-youtube-play"></i>
                                            <div>7,820</div>
                                            <p>Subscribers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="twitter">
                                            <i class="fa fa-twitter"></i>
                                            <div>1,562</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="pint">
                                            <i class="fa fa-pinterest"></i>
                                            <div>1,310</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                </ul> <!-- /.social icon -->     
                            </div>
                            <!-- END OF /. SOCIAL ICON -->
                            <!-- START ADVERTISEMENT -->
                            <div class="add-inner">
                                <img src="assets/images/add320x270-1.jpg" class="img-responsive" alt="">
                            </div>
                            <!-- END OF /. ADVERTISEMENT -->
                            <!-- START NAV TABS -->
                            <div class="tabs-wrapper">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Most Viewed</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Popular news</a></li>
                                </ul>
                                <!-- Tab panels one --> 
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="home">

                                        <div class="most-viewed">
                                            <ul id="most-today" class="content tabs-content">
                                                <li><span class="count">01</span><span class="text"><a href="#">South Africa bounce back on eventful day</a></span></li>
                                                <li><span class="count">02</span><span class="text"><a href="#">Steyn ruled out of series with shoulder fracture</a></span></li>
                                                <li><span class="count">03</span><span class="text"><a href="#">BCCI asks ECB to bear expenses of team's India tour</a></span></li>
                                                <li><span class="count">04</span><span class="text"><a href="#">Duminy, Elgar tons set Australia huge target</a></span></li>
                                                <li><span class="count">05</span><span class="text"><a href="#">English spinners are third-class citizens, says Graeme Swann</a></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Tab panels two --> 
                                    <div role="tabpanel" class="tab-pane fade" id="profile">
                                        <div class="popular-news">
                                            <div class="p-post">
                                                <h4><a href="#">It is a long established fact that a reader will be distracted by  </a></h4>
                                                <ul class="authar-info">
                                                    <li><a href="#" class="link"><i class="ti-timer"></i> May 15, 2016</a></li>
                                                    <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                                </ul>
                                                <div class="reatting-2">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <div class="p-post">
                                                <h4><a href="#">It is a long established fact that a reader will be distracted by  </a></h4>
                                                <ul class="authar-info">
                                                    <li><a href="#" class="link"><i class="ti-timer"></i> May 15, 2016</a></li>
                                                    <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                                </ul>
                                                <div class="reatting-2">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <div class="p-post">
                                                <h4><a href="#">It is a long established fact that a reader will be distracted by  </a></h4>
                                                <ul class="authar-info">
                                                    <li><a href="#" class="link"><i class="ti-timer"></i> May 15, 2016</a></li>
                                                    <li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>
                                                </ul>
                                                <div class="reatting-2">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END OF /. NAV TABS -->
                        </div>
                    </div>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
   

   @endsection