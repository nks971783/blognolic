@extends('layouts.app')

@section('title', 'Blognolic :: Home')

@section('meta_title', 'Home Page Meta Title')
@section('meta_keywords', 'Meta Keyword')
@section('meta_description', 'Meta Description')

@section('content')

<!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <!-- START NEWSTRICKER -->
            <div class="container">
                <div class="newstricker_inner">
                    <div class="trending"><strong>Trending</strong> Now</div>
                    <div id="NewsTicker" class="owl-carousel owl-theme">
                        
                       @foreach($trending as $blog)
                        <div class="item">
                            <a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a>
                        </div>
                        @endforeach
                       
                    </div>
                </div>
            </div>
            <!--  END OF /. NEWSTRICKER -->
            <!-- START POST BLOCK SECTION -->
            <section class="slider-inner">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="col-xs-12 col-sm-8 col-md-8 thm-padding">
                            <div class="slider-wrapper">
                                <div id="owl-slider" class="owl-carousel owl-theme">
                                    <!-- Slider item one -->
                                    @foreach($featured as $blog)
                                    <div class="item">
                                        <div class="slider-post post-height-1">
                                            <a href="{{url("/blog-detail/{$blog->slug}")}}" class="news-image"><img src="{{Storage::disk('s3')->url($blog->image)}}" alt="" class="img-responsive"></a>
                                            <div class="post-text">
                                                <span class="post-category">{{$blog->category->name}}</span>
                                                <h2><a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a></h2>
                                                <ul class="authar-info">
                                                    <li class="authar"><a href="#">by {{$blog->user->name}}</a></li>
                                                    <li class="date">{{ date('M d, Y', strtotime($blog->published_at)) }}</li>
                                                    <li class="view"><a href="#">{{$blog->views()->count()}} views</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 thm-padding">
                            <div class="row slider-right-post thm-margin">
                                @foreach($side_blogs as $blog)
                                <div class="col-xs-6 col-sm-12 col-md-12 thm-padding">
                                    <div class="slider-post post-height-2">
                                        <a href="{{url("/blog-detail/{$blog->slug}")}}" class="news-image"><img src="{{Storage::disk('s3')->url($blog->thumbnail)}}" alt="" class="img-responsive"></a>
                                        <div class="post-text">
                                            <span class="post-category">{{$blog->category->name}}</span>
                                            <h4><a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a></h4>
                                            <ul class="authar-info">
                                                <li class="authar hidden-xs hidden-sm"><a href="#">by {{$blog->user->name}}</a></li>
                                                <li class="hidden-xs">{{$blog->published_date}}</li>
                                                <li class="view hidden-xs hidden-sm"><a href="#">{{$blog->views()->count()}} views</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>

             <section class="slider-inner">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="post-head">
                            <h2 class="title">Most Reviewed Blogs</h2>        
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                            <div class="row slider-right-post thm-margin">
                                @foreach($reviewed as $review)
                                <div class="col-xs-12 col-sm-4 col-md-4 thm-padding">
                                    <div class="item">
                                        <div class="featured-post">
                                            <a href="{{url("/blog-detail/{$review->slug}")}}" class="news-image"><img src="{{Storage::disk('s3')->url($review->thumbnail)}}" alt="" class="img-responsive"></a>
                                            <div class="reatting">
                                                <?php
                                                $stared = floor($review->aggregate);
                                                $no_star = 5 - $stared;
                                                ?>
                                                {!!str_repeat('<i class="fa fa-star"></i>',$stared)!!}
                                               {!!str_repeat('<i class="fa fa-star-o"></i>',$no_star)!!}
                                              
                                            </div>
                                            <div class="post-text">
                                                <span class="post-category">{{$review->category->name}}</span>
                                                <h4>{{$review->title}}</h4>
                                                <ul class="authar-info">
                                                    <li><i class="ti-timer"></i>{{$review->published_date}}</li>
                                                    <li class="like"></i>{{$review->views()->count()}} views</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </main>
        
@endsection