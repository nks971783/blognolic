@extends('layouts.app')

@section('title', 'Blognolic :: About Us')

@section('meta_title', 'Home Page Meta Title')
@section('meta_keywords', 'Meta Keyword')
@section('meta_description', 'Meta Description')

@section('content')
        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <!-- START PAGE HEADER --> 
            <section class="inner-head" style=" background-image: url(assets/images/about-bg.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="entry-title">About Us</h1>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.
                            </p>
                            <div class="breadcrumb">
                                <ul class="clearfix">
                                    <li class="ib"><a href="index.html">Home</a></li>
                                    <li class="ib current-page">About</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END OF /. PAGE HEADER --> 
            <div class="team about-content">
                <div class="container">
                    <div class="about-title">
                        <h1>Our Mission</h1>
                        <h3>It is a long established fact that a reader will be distracted</h3>

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap 
                            into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum 
                            passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five .</p>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>Our Valuable Team Members </h2>
                        </div>
                        <!-- end col-12 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <figure class="member"> <img src="assets/images/team/1.png" class="img-responsive" alt="Image">
                                <figcaption>
                                    <h4>Debora Hilton</h4>
                                    <small>Editor</small>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- end member --> 
                        </div>
                        <!-- end col-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <figure class="member"> <img src="assets/images/team/2.png" class="img-responsive" alt="Image">
                                <figcaption>
                                    <h4>Debora Hilton</h4>
                                    <small>Editor</small>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- end member --> 
                        </div>
                        <!-- end col-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <figure class="member"> <img src="assets/images/team/3.png" class="img-responsive" alt="Image">
                                <figcaption>
                                    <h4>Chris O'Daniel</h4>
                                    <small>Publisher</small>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- end member --> 
                        </div>
                        <!-- end col-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <figure class="member"> <img src="assets/images/team/4.png" class="img-responsive" alt="Image">
                                <figcaption>
                                    <h4>Lian Holden</h4>
                                    <small>Project Manager</small>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </figcaption>
                            </figure>
                            <!-- end member --> 
                        </div>
                        <!-- end col-3 --> 
                    </div>
                    <!-- end row --> 

                    <div class="about-title">
                        <h2>Bold History that Fuels the Future</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap 
                            into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum 
                            passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap 
                            into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum 
                            passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    <h2>Related Articles</h2>
                    <div class="news-grid-2">
                        <div class="row">
                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="grid-item">
                                    <div class="grid-item-img">
                                        <a href="#">
                                            <img src="assets/images/218x150-1.jpg" class="img-responsive" alt="">
                                            <div class="link-icon"><i class="fa fa-play"></i></div>
                                        </a>
                                    </div>
                                    <h5><a href="#" class="title">Lorem Ipsum is simply dummy text of the printing.</a></h5>
                                    <ul class="authar-info">
                                        <li>May 15, 2016</li>
                                        <li class="hidden-sm"><a href="#" class="link">15 likes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3">
                                <div class="grid-item">
                                    <div class="grid-item-img">
                                        <a href="#">
                                            <img src="assets/images/218x150-2.jpg" class="img-responsive" alt="">
                                            <div class="link-icon"><i class="fa fa-camera"></i></div>
                                        </a>
                                    </div>
                                    <h5><a href="#" class="title">It is a long established fact that a reader will be distracted by</a></h5>
                                    <ul class="authar-info">
                                        <li>May 15, 2016</li>
                                        <li class="hidden-sm"><a href="#" class="link">15 likes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hidden-xs col-sm-3 col-md-3">
                                <div class="grid-item">
                                    <div class="grid-item-img">
                                        <a href="#">
                                            <img src="assets/images/218x150-3.jpg" class="img-responsive" alt="">
                                            <div class="link-icon"><i class="fa fa-camera"></i></div>
                                        </a>
                                    </div>
                                    <h5><a href="#" class="title">There are many variations of passages of Lorem Ipsum.</a></h5>
                                    <ul class="authar-info">
                                        <li>May 15, 2016</li>
                                        <li class="hidden-sm"><a href="#" class="link">15 likes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hidden-xs col-sm-3 col-md-3">
                                <div class="grid-item">
                                    <div class="grid-item-img">
                                        <a href="#">
                                            <img src="assets/images/218x150-4.jpg" class="img-responsive" alt="">
                                            <div class="link-icon"><i class="fa fa-camera"></i></div>
                                        </a>
                                    </div>
                                    <h5><a href="#" class="title">There are many variations of passages of Lorem Ipsum.</a></h5>
                                    <ul class="authar-info">
                                        <li>May 15, 2016</li>
                                        <li class="hidden-sm"><a href="#" class="link">15 likes</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
@endsection