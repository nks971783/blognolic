@extends('layouts.app')

@section('title', 'Blognolic :: Detail')

@section('meta_title', 'Home Page Meta Title')
@section('meta_keywords', 'Meta Keyword')
@section('meta_description', 'Meta Description')

@section('content')
      <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <!-- START PAGE TITLE --> 
            <div class="page-title">
                <!--<div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <h1><strong>{{$data->title}}</strong></h1>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ol class="breadcrumb">
                                <li><a href="">Home</a></li>
                                <li class="active"><a href="{{url("/blog-detail/{$data->slug}")}}">{{$data->title}}</a></li>
                            </ol>
                        </div>
                    </div>
                </div>-->
            </div>
            <!-- END OF /. PAGE TITLE --> 
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-8 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post_details_inner">
                                <div class="post_details_block">
                                    <figure class="social-icon">
                                        <img src="{{Storage::disk('s3')->url($data->image)}}" class="img-responsive" alt=""/>
                                        <div>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" class="hidden-xs"><i class="fa fa-linkedin"></i></a>
                                            <a href="#" class="hidden-xs"><i class="fa fa-pinterest-p"></i></a>
                                        </div>          
                                    </figure>
                                    <h2>{{$data->title}}</h2>
                                    <ul class="authar-info">
                                        <li><a href="#" class="link">{{$data->user->name}}</a></li>
                                        <li>{{ date('M d, Y', strtotime($data->published_at)) }}</li>
                                        <li><a href="#" class="link">{{$data->views()->count()}} views</a></li>
                                    </ul>
                                    <div class="content">
                                        <style>
                                            .content div{
                                              width:100% !important;
                                            }
                                            .content img{
                                                width:100% !important;
                                                height:auto !important;

                                            }

                                            .content section{
                                                width:100% !important;
                                               

                                            }

                                            .content a[rel~="noopener"]{
                                                display: none
                                            }
                                        </style>
                                     {!!$data->description!!}
                                    </div>
                                </div>
                                <!-- Post footer -->
                                <!-- <div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                           
                                            <ul class="pagination">
                                                <li class="disabled"><span class="ti-angle-left"></span></li>
                                                <li class="active"><span>1</span></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li class="disabled"><span class="extend">...</span></li><li>
                                                </li><li><a href="#">12</a></li>
                                                <li><a href="#"><i class="ti-angle-right"></i></a></li>
                                            </ul> 
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!-- START RELATED ARTICLES -->
                            @if(count($related_blogs) > 0)
                            <div class="post-inner post-inner-2">
                                <!--post header-->
                                <div class="post-head">
                                    <h2 class="title"><strong>Related </strong> Articles</h2>
                                </div>
                                <!-- post body -->
                                <div class="post-body">
                                    <div id="post-slider-2" class="owl-carousel owl-theme">
                                        <!-- item one -->
                                    
                                        <div class="item">
                                            <div class="news-grid-2">
                                                <div class="row row-margin">
                                                    <?php
                                                        
                                                        foreach ($related_blogs->take(3) as $key => $blog) {
                                                           
                                                    ?>
                                                    <div class="col-xs-6 col-sm-4 col-md-4 col-padding">
                                                        <div class="grid-item">
                                                            <div class="grid-item-img">
                                                                <a href="{{url("/blog-detail/{$blog->slug}")}}">
                                                                    <img src="{{Storage::disk('s3')->url($blog->thumbnail)}}" class="img-responsive" alt="">
                                                                    <!--<div class="link-icon"><i class="fa fa-play"></i></div>-->
                                                                </a>
                                                            </div>
                                                            <h5><a href="{{ url('blog-detail/'.$blog->slug) }}" class="title">{{$blog->title}}</a></h5>
                                                            <ul class="authar-info">
                                                                <li>{{ date('M d, Y', strtotime($blog->published_at)) }}</li>
                                                                <!--<li class="hidden-sm"><a href="#" class="link">15 likes</a></li>-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                        @if(count($related_blogs) > 3)
                                        <div class="item">
                                            <div class="news-grid-2">
                                                <div class="row row-margin">
                                                    <?php
                                                        
                                                        foreach ($related_blogs->skip(3)->take(3) as $key => $blog) {
                                                           
                                                    ?>
                                                    <div class="col-xs-6 col-sm-4 col-md-4 col-padding">
                                                        <div class="grid-item">
                                                            <div class="grid-item-img">
                                                                <a href="{{url("/blog-detail/{$blog->slug}")}}">
                                                                    <img src="{{Storage::disk('s3')->url($blog->thumbnail)}}" class="img-responsive" alt="">
                                                                    <!--<div class="link-icon"><i class="fa fa-play"></i></div>-->
                                                                </a>
                                                            </div>
                                                            <h5><a href="{{ url('blog-detail/'.$blog->slug) }}" class="title">{{$blog->title}}</a></h5>
                                                            <ul class="authar-info">
                                                                <li>{{ date('M d, Y', strtotime($blog->published_at)) }}</li>
                                                                <!--<li class="hidden-sm"><a href="#" class="link">15 likes</a></li>-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <!-- Post footer -->
                                <!--<div class="post-footer">
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-8 col-md-9 thm-padding">
                                            <a href="#" class="more-btn">More popular posts</a>
                                        </div>
                                        <div class="hidden-xs col-sm-4 col-md-3 thm-padding">
                                            <div class="social">
                                                <ul>
                                                    <li>
                                                        <div class="share transition">
                                                            <a href="#" target="_blank" class="ico fb"><i class="fa fa-facebook"></i></a>
                                                            <a href="#" target="_blank" class="ico tw"><i class="fa fa-twitter"></i></a>
                                                            <a href="#" target="_blank" class="ico gp"><i class="fa fa-google-plus"></i></a>
                                                            <a href="#" target="_blank" class="ico pin"><i class="fa fa-pinterest"></i></a>
                                                            <i class="ti-share ico-share"></i>
                                                        </div> 
                                                    </li>
                                                    <li><a href="#"><i class="ti-heart"></i></a></li>
                                                    <li><a href="#"><i class="ti-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                            @endif
                            <!-- END OF /. RELATED ARTICLES -->
                            <!-- START COMMENT -->
                            <!--<div class="comments-container">
                                <h3>Comments (3)</h3>
                                <ul class="comments-list">
                                    <li>
                                        <div class="comment-main-level">
                                            
                                            <div class="comment-avatar"><img src="{{ asset('assets/images/avatar-1.jpg') }}" alt=""></div>
                                            <div class="comment-box">
                                                <div class="comment-content">
                                                    <div class="comment-header"> <cite class="comment-author">- Mozammel Hoque</cite>
                                                        <time datetime="2012-10-27" class="comment-datetime">25 October 2016 at 12.27 pm</time>
                                                    </div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?</p>
                                                    <a href="#"  class="btn btn-news"> Reply</a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="comments-list reply-list">
                                            <li>
                                                
                                                <div class="comment-avatar"><img src="{{ asset('assets/images/avatar-1.jpg') }}" alt=""></div>
                                                <div class="comment-box">
                                                    <div class="comment-content">
                                                        <div class="comment-header"> <cite class="comment-author">- Tahmina Akthr</cite>
                                                            <time datetime="2012-10-27" class="comment-datetime">25 October 2016 at 12.27 pm</time>
                                                        </div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?</p>
                                                        <a href="#"  class="btn btn-news"> Reply</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                
                                                <div class="comment-avatar"><img src="{{ asset('assets/images/avatar-1.jpg') }}" alt=""></div>
                                                <div class="comment-box">
                                                    <div class="comment-content">
                                                        <div class="comment-header"> <cite class="comment-author">- Mozammel Hoque</cite>
                                                            <time datetime="2012-10-27" class="comment-datetime">25 October 2016 at 12.27 pm</time>
                                                        </div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?</p>
                                                        <a href="#"  class="btn btn-news"> Reply</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="comment-main-level">
                                            
                                            <div class="comment-avatar"><img src="{{ asset('assets/images/avatar-1.jpg') }}" alt=""></div>
                                            <div class="comment-box">
                                                <div class="comment-content">
                                                    <div class="comment-header"> <cite class="comment-author">- Tahmina Akthr</cite>
                                                        <time datetime="2012-10-27" class="comment-datetime">25 October 2016 at 12.27 pm</time>
                                                    </div>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?</p>
                                                    <a href="#"  class="btn btn-news"> Reply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>-->
                            <!-- END OF /. COMMENT -->
                            <!-- START COMMENTS FORMS -->
                            <!--<form class="comment-form" action="#" method="post">
                                <h3><strong>Leave</strong> a Comment</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">full name*</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Your name*">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="email">Email*</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Your email address here">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">website</label>
                                            <input type="text" class="form-control" id="website" name="website" placeholder="Your website url">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="email">Subject</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Write subject here">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">message</label>
                                    <textarea class="form-control" id="message" name="message" placeholder="Your Comment*" rows="5"></textarea>
                                </div>
                                <a href="#"  class="btn btn-news"> Submit</a>
                            </form>-->
                            <!-- END OF /. COMMENTS FORMS -->
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    <!-- START SIDE CONTENT -->
                    <div class="col-sm-4 col-p rightSidebar">
                        <div class="theiaStickySidebar">
                            <!-- START ADVERTISEMENT -->
                            <div class="add-inner">
                                <img src="{{ asset('assets/images/add320x270-1.jpg') }}" class="img-responsive" alt="">
                            </div>
                            <!-- END OF /. ADVERTISEMENT -->
                            <!-- START SOCIAL ICON -->
                            <!--<div class="social-media-inner">
                                <ul class="social-media clearfix">
                                    <li>
                                        <a href="#" class="rss">
                                            <i class="fa fa-rss"></i>
                                            <div>2,035</div>
                                            <p>Subscribers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="fb">
                                            <i class="fa fa-facebook"></i>
                                            <div>3,794</div>
                                            <p>Fans</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="g_plus">
                                            <i class="fa fa-google-plus"></i>
                                            <div>941</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="you_tube">
                                            <i class="fa fa-youtube-play"></i>
                                            <div>7,820</div>
                                            <p>Subscribers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="twitter">
                                            <i class="fa fa-twitter"></i>
                                            <div>1,562</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="pint">
                                            <i class="fa fa-pinterest"></i>
                                            <div>1,310</div>
                                            <p>Followers</p>
                                        </a>
                                    </li>
                                </ul>    
                            </div>-->
                            <!-- END OF /. SOCIAL ICON -->
                            <!-- START NAV TABS -->
                            <div class="tabs-wrapper">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Most Viewed</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Popular news</a></li>
                                </ul>
                                <!-- Tab panels one --> 
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="home">

                                        <div class="most-viewed">
                                            <ul id="most-today" class="content tabs-content">
                                                <?php $i = 1; ?>
                                                @foreach($most_views as $list)
                                                <li><span class="count">0{{$i}}</span><span class="text"><a href="{{url("/blog-detail/{$list->slug}")}}">{{ $list->title }}</a></span></li>
                                                <?php $i++; ?>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Tab panels two --> 
                                    <div role="tabpanel" class="tab-pane fade" id="profile">
                                        <div class="popular-news">
                                        @foreach($most_rating as $list)
                                            <div class="p-post">
                                                <h4><a href="{{url("/blog-detail/{$list->slug}")}}">{{ $list->title }}</a></h4>
                                                <ul class="authar-info">
                                                    <li><a href="{{url("/blog-detail/{$list->slug}")}}" class="link"><i class="ti-timer"></i>{{ date('M d, Y', strtotime($list->published_at)) }}</a></li>
                                                    <!--<li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>-->
                                                </ul>
                                                <div class="reatting-2">
                                                    <?php
                                                    $stared = floor($list->aggregate);
                                                    $no_star = 5 - $stared;
                                                    ?>
                                                    {!!str_repeat('<i class="fa fa-star"></i>',$stared)!!}
                                                   {!!str_repeat('<i class="fa fa-star-o"></i>',$no_star)!!}
                                                </div>
                                            </div>
                                         @endforeach   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END OF /. NAV TABS -->
                        </div>
                    </div>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
   
   @endsection