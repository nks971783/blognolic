<!DOCTYPE html>
<html lang="en">
    
    <head>
        @include('includes.head')
    </head>

    <body>
        <!-- PAGE LOADER -->
        <div class="se-pre-con"></div>
            <header>
                @include('includes.header')
            </header>
                @yield('content')
            <footer>
                @include('includes.footer')
            </footer>
            <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <div class="copy">Copyright@<?php echo date('Y'); ?> Blognolic.</div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <ul class="footer-nav">
                            <li class="{{ ( Request::is('contact-us')? 'active' : '') }}"><a href="{{ url('contact-us') }}">Contact</a></li>
                            <li class="{{ ( Request::is('/')? 'active' : '') }}"><a href="{{ url('/') }}">Home</a></li>
                            <!--<li class="{{ ( Request::is('about-us')? 'active' : '') }}"><a href="{{ url('about-us') }}">About Us</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="{{ asset('js/all.js') }}" type="text/javascript"></script>
    </body>
</html>