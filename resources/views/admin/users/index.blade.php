@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Users</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span12">
      	@if(session('success'))
	      	<div class="alert alert-success">
	              <button class="close" data-dismiss="alert">×</button>
	              <strong>Success!</strong> {{ session('success') }}
	        </div>
         @endif
      	<a href="{{ url('admin/add-user') }}" class="btn btn-primary">Add New</a>
      	<div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Users List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered" id="userResult">
              <thead>
                <tr>
                  <th>ID</th>
                  <th width="300px">Name</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php $i=1; ?>
              	@if(count($users) > 0)
              		@foreach($users as $user)
		                <tr id="row_id{{ $user->id }}" class="gradeX">
		                  <td>{{ $i }}</td>
		                  <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
		                  <td class="center">
		                  	<a href="{{ url('admin/edit-user/'.$user->id) }}" class="btn btn-warning btn-mini">Edit</a>
		                  	<a href="javascript:void(0);" onclick="showPopup('<?php echo url('admin/delete-user/'.$user->id); ?>','<?php echo $user->id; ?>','userResult')" class="btn btn-danger btn-mini">Delete</a>
		                  </td>
		                </tr>
		                <?php $i++; ?>
	                @endforeach
	            @else

	            	<tr><td colspan="3">No Result Found!!</td></tr>

	            @endif
              </tbody>
            </table>
          </div>
        </div>

      </div>
	</div>

  </div>

</div>
@endsection
