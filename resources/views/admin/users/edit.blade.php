@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Users</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span6">

        @if(session('success'))
          <div class="alert alert-success">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Success!</strong> {{ session('success') }}
          </div>
         @endif
      	
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit User</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('admin/update-user/'.$user->id) }}" method="post" class="form-horizontal">
            @csrf
            
            <div class="control-group">
              <label class="control-label">User Name :</label>
              <div class="controls">
                <input type="text" class="span11" name="name" value="{{ $user->name }}" placeholder="User name" />
                @if ($errors->has('name')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('name') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input type="text" class="span11" name="email" value="{{ $user->email }}" placeholder="Email Address" />
                @if ($errors->has('email')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('email') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Select Role</label>
              <div class="controls">
                <select name="role">
                  @foreach($roles as $role)
                    <option <?php if($role->id == $user->role_id){ echo 'selected'; } ?> value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('role')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('role') }}</span>
                @endif
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>

      </div>
	   </div>

  </div>

</div>
@endsection
