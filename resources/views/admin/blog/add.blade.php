@extends('layouts.admin_app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pixelarity.css') }}">
<script type="text/javascript" src="{{ asset('assets/js/pixelarity-face.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/script-face.js') }}"></script>
<style type="text/css">
      #result{
        display: block;
        position: relative;
        margin-top: 40px;
      }
      .face{
        position: absolute;
        height: 0px;
        width: 0px;
        background-color: transparent;;
        border: 4px solid rgba(10,10,10,0.5);
      }
      .note-toolbar.panel-heading {
        z-index: 0;
      }
    </style>
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Blog</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span10">
      	
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Create New Blog</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('admin/create-blog') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <div class="control-group">
              <label class="control-label">Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="title" value="{{ old('title') }}" placeholder="Title" />
                @if ($errors->has('title')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('title') }}</span>
                @endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Select Category</label>
              <div class="controls">
                <select name="category">
                  @foreach($categories as $category)
                    <option <?php if(old('category') == $category->id){ ?> selected <?php } ?> value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select><br/>
                @if ($errors->has('category')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('category') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Image*: </label>
              <div class="controls">
                
                <input name="image" id="image" type="file" /><br/>
                <img style="width:50px;height:50px" id="result">
                <input id="cropped_image" name="crop_image" type="hidden" value="" >
                @if ($errors->has('image')) 
                      <span for="required" generated="true" class="help-inline error">{{ $errors->first('image') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Thumbnail*: </label>
              <div class="controls">
                
                <input name="thumbnail" id="thumbnail" type="file" /><br/>
                <img style="width:50px;height:50px" id="thumbnail_result">
                <input id="cropped_thumbnail" name="crop_thumbnail" type="hidden" value="" >
                <div class="uploaded-success"></div>
                @if ($errors->has('thumbnail')) 
                      <span for="required" generated="true" class="help-inline error">{{ $errors->first('thumbnail') }}</span>
                @endif
              </div>
            </div>
            

            <div class="control-group">
              <label class="control-label">Content :</label>
                <div class="controls">
                    <textarea id="summernote" name="description">{{ old('description') }}</textarea>
                  @if ($errors->has('description')) 
                    <span for="required" generated="true" class="help-inline error">{{ $errors->first('description') }}</span>
                  @endif
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Status</label>
              <div class="controls">
                <select name="status">
                    <option <?php if(old('status') == '1'){ ?> selected <?php } ?> value="1">Active</option>
                    <option <?php if(old('status') == '0'){ ?> selected <?php } ?> value="0">Inactive</option>
                </select><br/>
                @if ($errors->has('status')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('status') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_title" value="{{ old('meta_title') }}" placeholder="Meta Title" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_keyword" value="{{ old('meta_keyword') }}" placeholder="Meta Keyword" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Description :</label>
                <div class="controls">
                  <textarea name="meta_description" value="{{ old('meta_description') }}" class="span12" rows="6" placeholder="Enter Description ..."></textarea>
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">Publish At:</label>
              <div class="controls">
                <div  data-date="2019-09-17" class="input-append date datepicker">
                  <input type="text" name="published_at" value="2019-09-17" value="{{ old('published_at') }}" data-date-format="mm-dd-yyyy" class="span11" >
                  <span class="add-on"><i class="icon-th"></i></span> </div>
                  @if ($errors->has('published_at')) 
                    <span for="required" generated="true" class="help-inline error">{{ $errors->first('published_at') }}</span>
                  @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Is Featured?</label>
              <div class="controls">
                <label>
                  <input type="radio" value="1" name="featured" <?php if(old('status') == '1'){ ?> checked <?php } ?> />
                  Yes</label>
                <label>
                  <input type="radio" value="0" <?php if(old('status') == '0'){ ?> checked <?php } ?> name="featured" />
                  No</label>
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
          </form>
        </div>
      </div>

      </div>
	   </div>

  </div>

</div>

@endsection
