@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">CMS</a> </div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span12">
      	@if(session('success'))
	      	<div class="alert alert-success">
	              <button class="close" data-dismiss="alert">×</button>
	              <strong>Success!</strong> {{ session('success') }}
	        </div>
         @endif
      	<a href="{{ url('admin/add-page') }}" class="btn btn-primary">Add New</a>
      	<div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Page List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered" id="cmsResult">
              <thead>
                <tr>
                  <th>ID</th>
                  <th width="300px">Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php $i=1; ?>
              	@if(count($pages) > 0)
              		@foreach($pages as $page)
		                <tr id="row_id{{ $page->id }}" class="gradeX">
		                  <td>{{ $i }}</td>
		                  <td>{{ $page->page_title }}</td>
		                  <td class="center">
		                  	<a href="{{ url('admin/edit-page/'.$page->id) }}" class="btn btn-warning btn-mini">Edit</a>
		                  </td>
		                </tr>
		                <?php $i++; ?>
	                @endforeach
	            @else

	            	<tr><td colspan="3">No Result Found!!</td></tr>

	            @endif
              </tbody>
            </table>
          </div>
        </div>

      </div>
	</div>

  </div>

</div>
@endsection
