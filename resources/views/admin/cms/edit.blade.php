@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Cms</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span6">

        @if(session('success'))
          <div class="alert alert-success">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Success!</strong> {{ session('success') }}
          </div>
         @endif
      	
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Cms</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('admin/update-page/'.$page->id) }}" method="post" class="form-horizontal">
            @csrf
            <div class="control-group">
              <label class="control-label">Page Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="name" value="{{ $page->page_title }}" placeholder="Page Title" />
                @if ($errors->has('name')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('name') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Page Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="slug" value="{{ $page->slug }}" placeholder="Page Slug" />
                @if ($errors->has('slug')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('slug') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_title" value="{{ $page->meta_title }}" placeholder="Meta Title" />
                @if ($errors->has('meta_title')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_title') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_keyword" value="{{ $page->meta_keyword }}" placeholder="Meta Keyword" />
                @if ($errors->has('meta_keyword')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_keyword') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Description :</label>
                <div class="controls">
                  <textarea name="meta_description" class="span12" rows="6" placeholder="Enter Description ...">{{ $page->meta_description }}</textarea>
                  @if ($errors->has('meta_description')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_description') }}</span>
                @endif
                </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>

      </div>
	   </div>

  </div>

</div>
@endsection
