@extends('layouts.admin_app')

@section('content')
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Category</a></div>
  </div>

  <div class="container-fluid">

  	<div class="row-fluid">
      <div class="span6">

        @if(session('success'))
          <div class="alert alert-success">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Success!</strong> {{ session('success') }}
          </div>
         @endif
      	
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Category</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('admin/update-category/'.$category->id) }}" method="post" class="form-horizontal">
            @csrf
            <div class="control-group">
              <label class="control-label">Category Name :</label>
              <div class="controls">
                <input type="text" class="span11" name="name" value="{{ $category->name }}" placeholder="Category name" />
                @if ($errors->has('name')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('name') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Title :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_title" value="{{ $category->meta_title }}" placeholder="Meta Title" />
                @if ($errors->has('meta_title')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_title') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                <input type="text" class="span11" name="meta_keyword" value="{{ $category->meta_keyword }}" placeholder="Meta Keyword" />
                @if ($errors->has('meta_keyword')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_keyword') }}</span>
                @endif
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Meta Description :</label>
                <div class="controls">
                  <textarea name="meta_description" class="span12" rows="6" placeholder="Enter Description ...">{{ $category->meta_description }}</textarea>
                  @if ($errors->has('meta_description')) 
                  <span for="required" generated="true" class="help-inline error">{{ $errors->first('meta_description') }}</span>
                @endif
                </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>

      </div>
	   </div>

  </div>

</div>
@endsection
