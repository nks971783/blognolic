@extends('layouts.app')

@section('title', 'Blognolic :: '.$data->name )

@section('meta_title', 'Category Page Meta Title')
@section('meta_keywords', 'Category Page Meta Keyword')
@section('meta_description', 'Category Page Meta Description')

@section('content')
        <!-- *** START PAGE MAIN CONTENT *** -->
        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <h1><strong>{{ $data->name }}</strong></h1>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li class="active"><a href="">{{ $data->name }}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF /. PAGE TITLE --> 
        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <!-- START POST BLOCK SECTION -->
            <section class="slider-inner">
                <div class="container">
                    <div class="row thm-margin">
                        <div class="col-xs-12 col-sm-8 col-md-8 thm-padding">
                            <div class="slider-wrapper">
                                <div id="owl-slider" class="owl-carousel owl-theme">
                                    <!-- Slider item one -->
                                    @foreach($featured_by_category as $blog)
                                        <div class="item">
                                            <div class="slider-post post-height-1">
                                                <a href="{{url("/blog-detail/{$blog->slug}")}}" class="news-image"><img src="{{Storage::disk('s3')->url($blog->image)}}" alt="" class="img-responsive"></a>
                                                <div class="post-text">
                                                    <span class="post-category">{{$blog->category->name}}</span>
                                                    <h2><a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a></h2>
                                                    <ul class="authar-info">
                                                        <li class="authar"><a href="#">by {{$blog->user->name}}</a></li>
                                                        <li class="date">{{ date('M d, Y', strtotime($blog->published_at)) }}</li>
                                                        <li class="view"><a href="#">{{$blog->views()->count()}} views</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <!-- /.Slider item one -->
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 thm-padding">
                            <div class="row slider-right-post thm-margin">

                                @foreach($category_blogs as $blog)
                                    <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                        <div class="slider-post post-height-2">
                                            <a href="{{url("/blog-detail/{$blog->slug}")}}" class="news-image"><img src="{{Storage::disk('s3')->url($blog->thumbnail)}}" alt="" class="img-responsive"></a>
                                            <div class="post-text">
                                                <span class="post-category">{{$blog->category->name}}</span>
                                                <h4><a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a></h4>
                                                <ul class="authar-info">
                                                    <li class="authar hidden-xs hidden-sm"><a href="#">by {{$blog->user->name}}</a></li>
                                                    <li class="hidden-xs">{{ date('M d, Y', strtotime($blog->published_at)) }}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END OF /. POST BLOCK SECTION -->
            <div class="container">
                <div class="row row-m">
                    <!-- START MAIN CONTENT -->
                    <div class="col-sm-12 col-p  main-content">
                        <div class="theiaStickySidebar">
                            <div class="post-inner categoty-style-1">
                                <!-- post body -->
                                <div class="post-body">
                                    <div class="row row-m">
                                    @foreach($blogs as $blog)
                                        <div class="col-sm-4 col-p">
                                            <article>
                                                <figure>
                                                    <a href="{{url("/blog-detail/{$blog->slug}")}}"><img src="{{Storage::disk('s3')->url($blog->thumbnail)}}" height="242" width="345" alt="" class="img-responsive"></a>
                                                    <span class="post-category">{{$blog->category->name}}</span>
                                                </figure>
                                                <div class="post-info">
                                                    <h3><a href="{{url("/blog-detail/{$blog->slug}")}}">{{$blog->title}}</a></h3>
                                                    <ul class="authar-info">
                                                        <li><i class="ti-timer"></i>{{ date('M d, Y', strtotime($blog->published_at)) }} </li>
                                                        <!--<li><a href="#" class="link"><i class="ti-thumb-up"></i>15 likes</a></li>-->
                                                    </ul>
                                                </div>
                                            </article>
                                        </div>
                                    @endforeach

                                    </div>
                                </div>
                                <!-- Post footer -->
                                <!--<div class="post-footer"> 
                                    <div class="row thm-margin">
                                        <div class="col-xs-12 col-sm-12 col-md-12 thm-padding">
                                            
                                            <ul class="pagination">
                                                <li class="disabled"><span class="ti-angle-left"></span></li>
                                                <li class="active"><span>1</span></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li class="disabled"><span class="extend">...</span></li><li>
                                                <li><a href="#">12</a></li>
                                                <li><a href="#"><i class="ti-angle-right"></i></a></li>
                                            </ul> 
                                        </div>
                                    </div>
                                </div>--> <!-- /.Post footer-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF /. MAIN CONTENT -->
                    
                        </div>
                    </div>
                    <!-- END OF /. SIDE CONTENT -->
                </div>
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
      
      @endsection