            <div class="container">
                <div class="row">
                    <!-- START FOOTER BOX (About) -->
                    <div class="col-sm-3 footer-box">
                        <div class="about-inner">
                            <img src="{{ asset('assets/images/new_logo.jpg') }}" class="img-responsive" alt=""/>
                            <p>Welcome to a place where ideas matter. Real Simple: Ideas, Tips, and Simple Ways to Make Life Even Easier</p>
                            <!--<ul>
                                <li><i class="ti-location-arrow"></i>A-92 Sector 63 Noida Uttar Pradesh, India.</li>
                                <li><i class="ti-mobile"></i>+91 97180-87484</li>
                                <li><i class="ti-email"></i>hello@webnolic.com</li>
                            </ul>-->
                        </div>
                    </div>
                    <!--  END OF /. FOOTER BOX (About) -->
                    <!-- START FOOTER BOX (Twitter feeds) -->
                    <div class="col-sm-3 footer-box">
                        <!-- <div class="twitter-inner">
                            <h3 class="wiget-title">twitter feeds</h3>
                            <ul class="margin-top-60">
                                <li>Typi non habent claritatem insitam est usus legent is iis qui facit claritatem. Investigatione <a href="https://t.co/erenthemeGHTQ">https://t.co/erenthemeGHTQ</a>
                                    <span><i class="ti-twitter"></i>12 days ago</span>
                                </li>
                                <li>Typi non habent claritatem insitam est usus legent is <a href="https://t.co/erenthemeGHTQ">https://t.co/erenthemeGHTQ</a>
                                    <span><i class="ti-twitter"></i>10 days ago</span>
                                </li>
                            </ul>
                        </div>-->
                    </div>
                    <!-- END OF /. FOOTER BOX (Twitter feeds) -->
                    <!-- START FOOTER BOX (Category) -->
                    <div class="col-sm-2 footer-box">
                        <h3 class="wiget-title">Category</h3>
                        <ul class="menu-services">
                            @foreach($category_list as $list)
                                <li class="{{ ( Request::is('category/'.$list->slug)? 'active' : '') }}">
                                    <a href="{{ url('category/'.$list->slug) }}">{{$list->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- END OF /. FOOTER BOX (Category) -->
                    <!-- START FOOTER BOX (Recent Post) -->
                    <div class="col-sm-4 footer-box">
                        <!-- <h3 class="wiget-title">Recent Post</h3>
                        <div class="footer-news-grid">
                            <div class="news-list-item">
                                <div class="img-wrapper">
                                    <a href="#" class="thumb">
                                        <img src="{{ asset('assets/images/115x85-1.jpg') }}" alt="" class="img-responsive">
                                        <div class="link-icon">
                                            <i class="fa fa-camera"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-info-2">
                                    <h5><a href="#" class="title">Cooking Recipes Anytime And Anywhere</a></h5>
                                    <ul class="authar-info">
                                        <li><i class="ti-timer"></i> May 15, 2016</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-list-item">
                                <div class="img-wrapper">
                                    <a href="#" class="thumb">
                                        <img src="{{ asset('assets/images/115x85-2.jpg') }}" alt="" class="img-responsive">
                                        <div class="link-icon">
                                            <i class="fa fa-camera"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-info-2">
                                    <h5><a href="#" class="title">Cooking Recipes Anytime And Anywhere</a></h5>
                                    <ul class="authar-info">
                                        <li><i class="ti-timer"></i> May 15, 2016</li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <!-- END OF /. FOOTER BOX (Recent Post) -->
                </div>
            </div>