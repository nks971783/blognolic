
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('title')</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         {!! SEO::generate() !!}

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{asset('assets/images/ico/favicon.png')}}" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('assets/images/ico/apple-touch-icon-57-precomposed.png')}}">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('assets/images/ico/apple-touch-icon-72-precomposed.png')}}">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('assets/images/ico/apple-touch-icon-114-precomposed.png')}}">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('assets/images/ico/apple-touch-icon-144-precomposed.png')}}">

     
        <!-- font awesome -->
        <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/css/flaticon.css')}}" rel="stylesheet" type="text/css"/>


        
        <link href="{{asset('css/all.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>


        <!-- themify-icons -->
        