
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <!-- Start header social -->
                            <div class="header-social">
                                <ul>
                                    <!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="hidden-xs"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                    <li class="hidden-xs"><a href="#"><i class="fa fa-vimeo"></i></a></li>-->
                                </ul>
                            </div>
                            <!-- End of /. header social -->
                            <!-- Start top left menu -->
                            <div class="top-left-menu">
                                <ul>
                                    <li class="{{ ( Request::is('contact-us')? 'active' : '') }}"><a href="{{ url('contact-us') }}">Contact</a></li>
                                    <li class="{{ ( Request::is('/')? 'active' : '') }}"><a href="{{ url('/') }}">Home</a></li>
                                    <!--<li class="{{ ( Request::is('about-us')? 'active' : '') }}"><a href="{{ url('about-us') }}">About Us</a></li>-->
                                </ul>
                            </div>
                            <!-- End of /. top left menu -->
                        </div>
                        <!-- Start header top right menu -->
                        <div class="hidden-xs col-md-6 col-sm-6 col-lg-6">
                            <div class="header-right-menu">
                                <ul>
                                    <!--<li>Currency: <a href="#">USD</a></li>
                                    <li>Wishlist: <a href="#">12</a></li>-->
                                    @guest
                                        <!--<li> <a href="{{ route('register') }}"><i class="fa fa-lock"></i> Sign Up </a>or<a href="{{ route('login') }}">   Login</a></li>-->
                                    @else
                                        <!--<li class="nav-item">
                                            <a class="nav-link" href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                                               <i class="fa fa-user"></i> {{ Auth::user()->name }}
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                        </li>-->
                                    @endguest

                                </ul>
                            </div>
                        </div> <!-- end of /. header top right menu -->
                    </div> <!-- end of /. row -->
                </div> <!-- end of /. container -->
            </div>
            <!-- END OF /. HEADER TOP SECTION -->
            <!-- START MIDDLE SECTION -->
            <div class="header-mid hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo">
                                <a href="{{ url('/') }}"><img src="{{ asset('assets/images/new_logo.jpg') }}" class="img-responsive" alt=""></a>

                            </div>
                        </div>
                        <div class="col-sm-8">
                            <a href="#"><img src="{{ asset('assets/images/add728x90-1.jpg') }}" class="img-responsive" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF /. MIDDLE SECTION -->
            <!-- START NAVIGATION -->
            <nav class="navbar navbar-default navbar-sticky navbar-mobile bootsnav">
                <!-- Start Top Search -->
                <div class="top-search">
                    <div class="container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <!-- End Top Search -->
                <div class="container">            
                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <!--<li class="search"><a href="#"><i class="fa fa-search"></i></a></li>-->
                        </ul>
                    </div>
                    <!-- End Atribute Navigation -->
                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#brand"><img src="{{ asset('assets/images/new_logo.jpg') }}" class="logo" alt=""></a>
                    </div>
                    <!-- End Header Navigation -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-left" data-in="" data-out="">
                            <li class="{{ ( Request::is('/')? 'active' : '') }}">
                                <a href="{{ url('/') }}">Home</a>
                            </li>
                            @foreach($category_list as $list)
                                <li class="{{ ( Request::is('category/'.$list->slug)? 'active' : '') }}">
                                    <a href="{{ url('category/'.$list->slug) }}">{{$list->name}}</a>
                                </li>
                            @endforeach
                            
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>