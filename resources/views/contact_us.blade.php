@extends('layouts.app')

@section('title', 'Blognolic :: Contact Us')

@section('meta_title', 'Home Page Meta Title')
@section('meta_keywords', 'Meta Keyword')
@section('meta_description', 'Meta Description')

@section('content')
        <!-- *** START PAGE MAIN CONTENT *** -->
        <!-- START PAGE TITLE --> 
        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <h1><strong>Contact</strong></h1>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li class="active"><a href="">Contact</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF /. PAGE TITLE --> 
        <!-- *** START PAGE MAIN CONTENT *** -->
        <main class="page_main_wrapper">
            <div class="container">
                <div class="row row-m">
                    <div class="col-sm-12 main-content col-p">
                        <div class="theiaStickySidebar">
                            <!-- START CONTACT FORM AREA -->
                            <div class="contact_form_inner">
                                <div class="panel_inner">
                                    <div class="panel_header">
                                        <h4><strong>We'd Love to Here</strong> Form you, Get in Touch With in Us? </h4>
                                    </div>
                                     @if(session()->has('success'))
                                        <h4 class="success">{{ session()->get('success') }}</h4>
                                    @endif
                                    <div class="panel_body">
                                        <form class="comment-form" action="{{ url('post-contact') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">full name*</label>
                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Your name*" required="true">
                                                        @if($errors->has('name'))
                                                            <span class="error">{{ $errors->first('name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="email">Email*</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="email" name="email" placeholder="Your email address here" required="true">
                                                        @if($errors->has('email'))
                                                            <span class="error">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">website</label>
                                                        <input type="text" class="form-control" id="website" name="website" placeholder="Your website url" required="true">
                                                        @if($errors->has('website'))
                                                            <span class="error">{{ $errors->first('website') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="email">Subject</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Write subject here" required="true">
                                                        @if($errors->has('subject'))
                                                            <span class="error">{{ $errors->first('subject') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">message</label>
                                                <textarea class="form-control" id="message" name="message" placeholder="Your Comment" rows="5"></textarea>
                                            </div>
                                            <input type="submit" name="submit" class="btn btn-news"  value="Submit" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- END OF CONTACT FORM AREA -->
                        </div>
                    </div>
                    <!--<div class="col-sm-4 rightSidebar col-p">
                        <div class="theiaStickySidebar">
                           
                            <div class="panel_inner">
                                <div class="panel_header">
                                    <h4><strong>Contact</strong> Info </h4>
                                </div>
                                <div class="panel_body">
                                    <address> <strong>Twitter, Inc.</strong><br> 1355 Market Street, Suite 900<br> San Francisco, CA 94103<br> <abbr title="Phone">P:</abbr> (123) 456-7890 </address>
                                    <address> <strong>Twitter, Inc.</strong><br> <abbr title="Phone">Phone:</abbr> 1.800.555.6789<br> Fax: 1.888.555.9876<br> <abbr title="Phone">P:</abbr> (123) 456-7890 </address>
                                    <address> <strong>Full Name</strong><br> <a href="mailto:#">first.last@example.com</a> </address>
                                </div>
                            </div>
                            
                        </div>
                    </div>-->
                </div>
                <!--<div class="panel_inner">
                    <div class="panel_body">
                        <div id="map"></div>
                    </div>
                </div>-->
            </div>
        </main>
        <!-- *** END OF /. PAGE MAIN CONTENT *** -->
        
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=%20AIzaSyB4iuUg1YDRIBRZ5e-jdssfqDuT9VLiOnY"></script>
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 10, scrollwheel: false,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(23.8103968, 90.41256666), //Dhaka

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "administrative.locality", "elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}]}, {"featureType": "administrative.locality", "elementType": "labels.icon", "stylers": [{"visibility": "on"}, {"color": "#f1c40f"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#eb0254"}, {"visibility": "on"}]}]
                };

                // image from external URL

                var myIcon = 'assets/images/marker.png';

                //preparing the image so it can be used as a marker
                //https://developers.google.com/maps/documentation/javascript/reference#Icon
                //includes hacky fix "size" to allow for wobble
                var catIcon = {
                    url: myIcon,
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(23.8103968, 90.41256666), //Dhaka
                    map: map,
                    icon: catIcon,
                    title: 'Snazzy!',
                    animation: google.maps.Animation.DROP,
                });
            }
            setTimeout(function(){ $('.success').fadeOut(); }, 5000);
        </script>
    
    @endsection