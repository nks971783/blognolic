<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function(){

	Route::get('/','Admin\DashboardController@dashboard');

	//Users

	Route::get('users','Admin\AdminController@users');
	Route::get('add-user','Admin\AdminController@addUser');
	Route::post('create-user','Admin\AdminController@createUser');
	Route::get('edit-user/{id}','Admin\AdminController@editUser');
	Route::post('update-user/{id}','Admin\AdminController@updateUser');
	Route::get('delete-user/{id}','Admin\AdminController@deleteUser');

	//Category

	Route::get('categories','Admin\CategoryController@categories');
	Route::get('add-category','Admin\CategoryController@addCategory');
	Route::post('create-category','Admin\CategoryController@createCategory');
	Route::get('edit-category/{id}','Admin\CategoryController@editCategory');
	Route::post('update-category/{id}','Admin\CategoryController@updateCategory');
	Route::get('delete-category/{id}','Admin\CategoryController@deleteCategory');

	//Blogs

	Route::get('blogs','Admin\BlogController@blogs');
	Route::get('add-blog','Admin\BlogController@addBlog');
	Route::post('create-blog','Admin\BlogController@createBlog');
	Route::get('edit-blog/{id}','Admin\BlogController@editBlog');
	Route::post('update-blog/{id}','Admin\BlogController@updateBlog');
	Route::get('delete-blog/{id}','Admin\BlogController@deleteBlog');


	// Views
	Route::get('add-view/{id}','Admin\ViewController@addView');


	// Reviews
	Route::get('add-review/{blog_id}/{rating}','Admin\ReviewController@addReview');

	// CMS
	Route::get('cms','Admin\CmsController@cms');
	Route::get('add-page','Admin\CmsController@addPage');
	Route::post('create-page','Admin\CmsController@createPage');
	Route::get('edit-page/{id}','Admin\CmsController@editPage');
	Route::post('update-page/{id}','Admin\CmsController@updatePage');

});

// Route::get('/','PageController@home');
Route::get('category/{cat?}','PageController@categoryBlog');
Route::get('about-us','PageController@aboutUs');
Route::get('contact-us','PageController@contactUs');
Route::get('popular-blogs','PageController@popularBlogs');
Route::get('blog-detail/{slug?}','PageController@blogDetail');
Route::post('post-contact','PageController@postContactUs');
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
