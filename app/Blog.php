<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Storage;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Str;
class Blog extends Model
{
    use Cachable;
    protected $guarded = [];

   

    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function views(){
        return $this->hasMany('App\View');
    }

    public function reviews(){
        return $this->hasMany('App\Review');
    }

     protected static function boot()
    {
        parent::boot();

        static::created(function ($blog) {
            $blog->update(['slug' => $blog->title]);
        });

    }
     public function setSlugAttribute($value)
    {
        if (static::whereSlug($slug = Str::slug($value))->exists()) {
            $slug = "{$slug}-{$this->id}";
        }
        $this->attributes['slug'] = $slug;
    }


    public function incrementSlug($slug)
    {
        // get the slug of the latest created post
        $max = static::whereTitle($this->title)->latest('id')->skip(1)->value('slug');

        if (is_numeric($max[-1])) {
            return pred_replace_callback('/(\d+)$/', function ($mathces) {
                return $mathces[1] + 1;
            }, $max);
        }

        return "{$slug}-2";
    }


    public function image(){
        //return Storage::disk('s3')->url($this->image);
        return $this->image;
    }


    public function scopeTrending($query,$limit=10){
       

    return $query->select(DB::raw('blogs.*, count(*) as `aggregate`'))
    ->join('views', 'blogs.id', '=', 'views.blog_id')
    ->groupBy('views.blog_id')
    ->orderBy('aggregate', 'desc')->limit($limit)
    ;
    }


    public function scopeHighReviewed($query,$limit=10){

       $query->select(DB::raw('blogs.*, avg(reviews.rating) as `aggregate`'))
        ->join('reviews', 'blogs.id', '=', 'reviews.blog_id')
        ->groupBy('blog_id')
        ->orderBy('aggregate', 'desc')->limit($limit);
    }








    
}
