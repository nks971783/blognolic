<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class View extends Model
{

	use Cachable;

    protected $guarded = [];

    public function blog(){
    	return $this->belongsTo('App\Blog');
    }
}
