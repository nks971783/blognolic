<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Blog;
use Mail;

class PageController extends Controller
{
    public function home(){
    	return view('index');
    }

    public function categoryBlog($category){
        $data = Category::where('slug',$category)->first();
        $blogs = Blog::where('category_id',$data->id)->where('featured','0')->orderBy('id','desc')->get();
        $featured_by_category = Blog::where('featured','1')->where('category_id',$data->id)->orderBy('id','desc')->limit(5)->get();
        $category_blogs = Blog::where('category_id','!=',$data->id)->where('featured','1')->groupBy('category_id')->orderBy('id','desc')->limit(2)->get();
    	return view('category',compact('data','blogs','featured_by_category','category_blogs'));
    }

    public function contactUs(){
    	return view('contact_us');
    }

    public function aboutUs(){
    	return view('about_us');
    }

    public function popularBlogs(){
    	return view('popular_blogs');
    }

    public function postContactUs(Request $request){
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'subject' => 'required',
        ]);

        $user['name'] = $request->name;
        $user['email'] = $request->email;
        $user['website'] = $request->website;
        $user['subject'] = $request->subject;
        $user['message'] = (isset($request->message) && $request->message != "")?$request->message:'-';

        Mail::to('nks971783@gmail.com')->send(new \App\Mail\ContactMail($user));
        return redirect('contact-us')->with('success','Thank you !! We will contact with you shortly.');

    }

    public function blogDetail($slug){
        $data = Blog::where('slug',$slug)->first();
        if(empty($data)){
            return redirect('/');
        }
        $related_blogs = Blog::where('category_id',$data->category_id)->where('id','!=',$data->id)->get();
        $most_views = Blog::trending()->get();
        $most_rating = Blog::HighReviewed()->get();

    	return view('blog_detail',compact('data','related_blogs','most_views','most_rating'));
    }
}
