<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $trending = Blog::trending()->get();
        $featured = Blog::where('featured','1')->get();
        $reviewed = Blog::HighReviewed()->get();

        /*
        $category_blogs = Category::all();
        foreach($category_blogs as $data){
            $blogs = Blog::where('category_id',$data->id)->orderBy('id','desc')->limit(5)->get();
            $data->blogs = $blogs;
        }
        */

        $side_blogs = Blog::where('featured','0')->limit(2)->orderBy('id','desc')->get();

        return view('index',compact('trending','featured','side_blogs','reviewed'));
    }
}
