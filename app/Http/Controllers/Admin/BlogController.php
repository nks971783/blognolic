<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlog;
use App\Http\Requests\UpdateBlog;
use App\{Blog, Category};
use Storage;
use Auth;
use Image;
use File;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function blogs(){
    	$blogs = Blog::paginate(10);
    	return view('admin/blog/index',compact('blogs'));
    }

    public function addBlog(){
    	$categories = Category::get();
    	return view('admin/blog/add',compact('categories'));
    }

    public function createBlog(CreateBlog $request){
        if($request->crop_image != ""){
            $base64 = $request->crop_image;
            list($baseType, $image) = explode(';', $base64);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $imageName = time().'.jpg';
            $filePath = 'blogs/image/' . $imageName;
            Storage::disk('s3')->put($filePath, $image, 'public');
        }

        if($request->crop_thumbnail != ""){
            $base64 = $request->crop_thumbnail;
            list($baseType, $image) = explode(';', $base64);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $imageName = time().'.jpg';
            $thumbnailPath = 'blogs/thumbnail/' . $imageName;
            Storage::disk('s3')->put($thumbnailPath, $image, 'public');
        }

        $slug = Str::slug($request->title);

    	Blog::create([
            'title' => $request->title,
            'slug' => $slug,
            'category_id' => $request->category,
            'description' => $request->description,
            'status' => $request->status,
            'image' => $filePath,
            'thumbnail' => $thumbnailPath,
            'user_id' => Auth::user()->id,
            'published_at' => $request->published_at,
            'featured' => $request->featured,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description
        ]);

        return redirect('admin/blogs')->with('success','New Blog Added.');
    }

    public function editBlog($id){
        $blog = Blog::find($id);
        $categories = Category::get();
        return view('admin/blog/edit',compact('categories','blog'));
    }

    public function updateBlog(UpdateBlog $request,$id){
        $blog = Blog::find($id);
        if($request->hasFile('image')){
            $image = time().'.'.$request->image->getClientOriginalExtension();
            $file = $request->file('image');
            $filePath = 'blogs/' . $image;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $old_image = $blog->image;
            $blog->image = $filePath;
        }

        if($request->crop_image != ""){
            $old_image = $blog->image;
            $base64 = $request->crop_image;
            list($baseType, $image) = explode(';', $base64);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $imageName = time().'.jpg';
            $filePath = 'blogs/image/' . $imageName;
            Storage::disk('s3')->put($filePath, $image, 'public');
            $blog->image = $filePath;
        }

        if($request->crop_thumbnail != ""){
            $old_thumbnail = $blog->thumbnail;
            $base64 = $request->crop_thumbnail;
            list($baseType, $image) = explode(';', $base64);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $imageName = time().'.jpg';
            $thumbnailPath = 'blogs/thumbnail/' . $imageName;
            Storage::disk('s3')->put($thumbnailPath, $image, 'public');
            $blog->thumbnail = $thumbnailPath;
        }

        $slug = Str::slug($request->title);

        $blog->title = $request->title;
        $blog->slug = $slug;
        $blog->category_id = $request->category;
        $blog->description = $request->description;
        $blog->status = $request->status;
        $blog->user_id = Auth::user()->id;
        $blog->published_at = $request->published_at;
        $blog->featured = $request->featured;
        $blog->meta_title = $request->meta_title;
        $blog->meta_keyword = $request->meta_keyword;
        $blog->meta_description = $request->meta_description;
        $blog->save();
        if($request->crop_image != ""){
            $exists = Storage::disk('s3')->exists($old_image);
            if($exists) {
                Storage::disk('s3')->delete($old_image);
            }
        }
        if($request->crop_thumbnail != ""){
            $exists = Storage::disk('s3')->exists($old_thumbnail);
            if($exists) {
                Storage::disk('s3')->delete($old_thumbnail);
            }
        }
        return back()->with('success','Blog Updated.');
    }

    public function deleteBlog($id){
        Blog::find($id)->delete();
        return response()->json(['status' => '1', 'message' => 'Success']);
    }
}
