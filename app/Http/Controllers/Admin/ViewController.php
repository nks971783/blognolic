<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\View;
use Auth;

class ViewController extends Controller
{
    public function addView($id){
    	$clientIP = \Request::ip();
    	$res = View::create([
    		'blog_id' => $id,
    		'ip_address' => $clientIP,
    		'user_id' => Auth::user()->id
    	]);
    	if($res){
    		return response()->json(['status' => '1','msg' => 'success']);
    	}else{
    		return response()->json(['status' => '0','msg' => 'error']);
    	}
    }
}
