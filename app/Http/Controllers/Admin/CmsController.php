<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms;

class CmsController extends Controller
{
    public function cms(){
    	$pages = Cms::get();
    	return view('admin/cms/index',compact('pages'));
    }

    public function addPage(){
    	return view('admin/cms/add');
    }

    public function createPage(Request $request){
    	$this->validate($request,[
		    'name' => 'required',
		    'slug' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required'
		]);
		Cms::create([
            'page_title' => $request->name,
            'slug' => $request->slug,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description
        ]);
    	return redirect('admin/cms')->with('success','New Page Added.');
    }

    public function editPage($id){
    	$page = Cms::find($id);
    	return view('admin/cms/edit',compact('page'));
    }

    public function updatePage(Request $request,$id){
    	$page = Cms::find($id);
    	$page->page_title = $request->name;
    	$page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_keyword = $request->meta_keyword;
        $page->meta_description = $request->meta_description;
    	$page->save();
    	return back()->with('success','Page Updated.');
    }

}
