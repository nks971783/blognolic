<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Hash;

class AdminController extends Controller
{
    public function users(){
    	$users = User::get();
    	return view('admin/users/index',compact('users'));
    }

    public function addUser(){
    	$roles = Role::get();
    	return view('admin/users/add',compact('roles'));
    }

    public function createUser(Request $request){
    	$this->validate($request,[
		    'name' => 'required',
		    'email' => 'required',
		    'password' => 'required',
		    'role' => 'required'
		]);
		User::create(['name' => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password), 'role_id' => $request->role ]);
    	return redirect('admin/users')->with('success','New User Added.');
    }

    public function editUser($id){
    	$user = User::find($id);
    	$roles = Role::get();
    	return view('admin/users/edit',compact('user','roles'));
    }

    public function updateUser(Request $request,$id){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'role' => 'required'
        ]);
    	$user = User::find($id);
    	$user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role;
    	$user->save();
    	return back()->with('success','User Updated.');
    }

    public function deleteUser($id){
    	User::find($id)->delete();
    	return response()->json(['status' => '1', 'message' => 'Success']);
    }
}
