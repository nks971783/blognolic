<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use Auth;

class ReviewController extends Controller
{
    public function addReview($blog_id,$rating){
    	$res = Review::create([
    		'blog_id' => $blog_id,
    		'rating' => $rating,
    		'user_id' => Auth::user()->id
    	]);
    	if($res){
    		return response()->json(['status' => '1','msg' => 'success']);
    	}else{
    		return response()->json(['status' => '0','msg' => 'error']);
    	}
    }
}
