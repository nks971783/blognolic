const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.styles([
    'public/assets/css/animage.min.css',
    'public/assets/css/bootstrap.min.css',
    // 'public/assets/css/flaticon.css',
    'public/assets/css/jquery-ui.min.css',
    'public/assets/css/RYPP.css',
    'public/assets/bootsnav/css/bootsnav.css',
    // 'public/assets/themify-icons/themify-icons.css',
    'public/assets/weather-icons/css/weather-icons.min.css',
    'public/assets/owl-carousel/owl.carousel.css',
    'public/assets/owl-carousel/owl.theme.css',
    'public/assets/owl-carousel/owl.transitions.css',
    'public/assets/css/style.css'
], 'public/css/all.css');


mix.scripts([
	'public/assets/js/jquery.min.js',
	'public/assets/js/jquery-ui.min.js',
	'public/assets/js/bootstrap.min.js',
	'public/assets/bootsnav/js/bootsnav.js',
	'public/assets/js/theia-sticky-sidebar.js',
    'public/assets/js/RYPP.js',
    'public/assets/owl-carousel/owl.carousel.min.js',
    'public/assets/js/custom.js',
	], 'public/js/all.js');